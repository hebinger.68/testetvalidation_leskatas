﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ProjetBowling.viewmodels;
using ProjetBowling.models;

namespace ProjetBowling
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        BowlingVM viewmodel = new BowlingVM();

        Partie partie;

        public MainWindow() {
            InitializeComponent();

            this.DataContext = viewmodel;

            partie = new Partie();
        }

        private void button_Click(object sender, RoutedEventArgs e) {
            try {
                partie.AddLancee(viewmodel.Nombre);
            } catch (Exception exception) {
                viewmodel.Message = exception.Message;
            }
            MiseAJourScore();
        }

        private void textBox_TextChanged(object sender, TextChangedEventArgs e) {
            int valeur;
            if (int.TryParse((sender as TextBox).Text, out valeur)) {
                viewmodel.Nombre = valeur;
            } else {
                viewmodel.Message = "Valeur n'est pas un nombre";
            }

        }

        private void MiseAJourScore() {

            List<BowlingVM.LigneTableauScores> scores = new List<BowlingVM.LigneTableauScores>();

            foreach (models.Frame frame in partie.frames) {
                GenereLigneTableau(scores, frame);
            }

            viewmodel.TableauScores = scores;

            if (partie.EstPartieTerminee()) {
                viewmodel.Message = "Partie Terminée avec " + partie.GetScore() + "pts GG !";
            }



        }

        private void GenereLigneTableau(List<BowlingVM.LigneTableauScores> scores, models.Frame frame) {
            BowlingVM.LigneTableauScores ligneTableauScores = new BowlingVM.LigneTableauScores();

            if (frame.GetState.Equals(models.Frame.FrameState.UNDEFINED)) {
                ligneTableauScores.Etat = "En cours";
            } else {
                ligneTableauScores.Etat = frame.GetState.ToString();
            }


            if (frame.lancees.Count == 1) {
                if (frame.GetState.Equals(models.Frame.FrameState.STRIKE)) {
                    ligneTableauScores.Lance1 = "X";
                } else {
                    ligneTableauScores.Lance1 = frame.lancees [0].ToString();
                }
            } else if (frame.lancees.Count == 2) {

                ligneTableauScores.Lance1 = frame.lancees [0].ToString();

                if (frame.GetState.Equals(models.Frame.FrameState.SPARE)) {
                    ligneTableauScores.Lance2 = "/";
                } else {
                    ligneTableauScores.Lance2 = frame.lancees [1].ToString();
                }
            }

            ligneTableauScores.Score = frame.GetScore().ToString();

            ligneTableauScores.Total = partie.GetScore().ToString();

            ligneTableauScores.Nombre = (partie.frames.IndexOf(frame) + 1).ToString();

            scores.Add(ligneTableauScores);
        }

        private void reset_Click(object sender, RoutedEventArgs e) {
            partie = new Partie();
            MiseAJourScore();
        }
    }
}
