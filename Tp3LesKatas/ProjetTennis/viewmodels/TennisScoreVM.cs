﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjetTennis.viewmodels
{
    public class TennisScoreVM : ABaseVM
    {
        private int _echangesJoueur1 = 0;
        public int EchangesJoueur1
        {
            get {
                return _echangesJoueur1;
            }
            set {
                _echangesJoueur1 = value;
                NotifyPropertyChanged();
            }
        }


        private int _echangesJoueur2 = 0;
        public int EchangesJoueur2
        {
            get {
                return _echangesJoueur2;
            }
            set {
                _echangesJoueur2 = value;
                NotifyPropertyChanged();
            }
        }


        private string _score = "-|-";
        public string Score
        {
            get {
                return _score;
            }
            set {
                _score = value;
                NotifyPropertyChanged();
            }
        }


        private string _message = "En attente de calcul";
        public string Message
        {
            get {
                return _message;
            }
            set {
                _message = value;
                NotifyPropertyChanged();
            }
        }
    }
}
