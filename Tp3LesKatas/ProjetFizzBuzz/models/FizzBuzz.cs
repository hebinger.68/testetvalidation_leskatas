﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectFizzBuzz
{
    public class FizzBuzz
    {

        public string Genere(int number) {
            if (!IsNumberValid(number)) {
                throw new Exception("Out of bounds");
            }

            List<int> series = GenerateSeries(number);

            return SeriesToFizzBuzzString(series);

        }

        private string SeriesToFizzBuzzString(List<int> series) {
            string result = "";
            foreach (int i in series) {
                if (i % 5 == 0 && i % 3 == 0) {
                    result += "FizzBuzz";
                } else
                if (i % 3 == 0) {
                    result += "Fizz";
                } else
                if (i % 5 == 0) {
                    result += "Buzz";
                } else {
                    result += i;
                }
            }

            return result;
        }

        private List<int> GenerateSeries(int number) {
            List<int> series = new List<int>();
            for (int i = 1; i <= number; i++) {
                series.Add(i);
            }
            return series;
        }

        public static bool IsNumberValid(int number) {
            return (number <= 150) && (number >= 15);
        }
    }


}
