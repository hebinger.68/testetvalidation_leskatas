﻿using ProjectFizzBuzz;
using ProjectFizzBuzz.viewmodels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Tp3LesKatas
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        FizzBuzzVM viewModel = new FizzBuzzVM();

        public MainWindow() {
            InitializeComponent();
            this.DataContext = viewModel;

        }

        private void button_Click(object sender, RoutedEventArgs e) {

            if (FizzBuzz.IsNumberValid(viewModel.ChoixNombre)) {
                FizzBuzz fizzBuzz = new FizzBuzz();
                viewModel.Resultat = fizzBuzz.Genere(viewModel.ChoixNombre);

            } else {
                viewModel.Resultat = "nombre invalide";
            }


        }
    }
}
