using System;
using Xunit;
using ProjectFizzBuzz;

namespace TestProject
{
    public class FizzBuzzTests
    {
        [Theory]
        [InlineData(0)]
        [InlineData(158)]
        [InlineData(null)]
        public void InvalidNumber(int number) {
            FizzBuzz fizzBuzz = new FizzBuzz();
            Assert.Throws<Exception>(() => fizzBuzz.Genere(number));
        }


        [Theory]
        [InlineData(15, "12Fizz4BuzzFizz78FizzBuzz11Fizz1314FizzBuzz")]
        [InlineData(16, "12Fizz4BuzzFizz78FizzBuzz11Fizz1314FizzBuzz16")]
        [InlineData(20, "12Fizz4BuzzFizz78FizzBuzz11Fizz1314FizzBuzz1617Fizz19Buzz")]
        public void GenereFizzBuzz(int number, string result) {
            FizzBuzz fizzBuzz = new FizzBuzz();
            Assert.Equal(fizzBuzz.Genere(number), result);
        }

    }
}
