﻿using ProjetBowling.models;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace TestProject
{
    public class BowlingTests
    {

        [Theory]
        [InlineData(-1)]
        [InlineData(11)]
        public void NombreInvalide(int nombre) {

            Frame frame = new Frame();

            Assert.Throws<Exception>(() => frame.AddLancee(nombre));
        }

        [Theory]
        [InlineData(new int [] { 10, 0 })]
        [InlineData(new int [] { 5, 6 })]
        public void NombreDepassement(int [] nombre) {

            Frame frame = new Frame();
            frame.AddLancee(nombre [0]);

            Assert.Throws<Exception>(() => frame.AddLancee(nombre [1]));
        }

        [Theory]
        [InlineData(new int [] { 0, 0 }, 0)]
        [InlineData(new int [] { 5, 5 }, 10)]
        [InlineData(new int [] { 10 }, 10)]
        [InlineData(new int [] { 3, 5 }, 8)]
        [InlineData(new int [] { 9 }, 9)]
        public void FrameScore(int [] lancees, int resultat) {

            Frame frame = new Frame();

            foreach (int lancee in lancees) {
                frame.AddLancee(lancee);
            }

            Assert.Equal(resultat, frame.GetScore());

        }

        [Fact]
        public void LimiteFrameScore() {

            Frame frame = new Frame();

            frame.AddLancee(10);

            Assert.Throws<Exception>(() => frame.AddLancee(0));

        }


        [Theory]
        [InlineData(new int [] { 0, 0 }, Frame.FrameState.TROU)]
        [InlineData(new int [] { 5, 5 }, Frame.FrameState.SPARE)]
        [InlineData(new int [] { 10 }, Frame.FrameState.STRIKE)]
        [InlineData(new int [] { 3, 5 }, Frame.FrameState.TROU)]
        [InlineData(new int [] { 8 }, Frame.FrameState.UNDEFINED)]
        public void EtatFrame(int [] lancees, Frame.FrameState resultat) {

            Frame frame = new Frame();

            foreach (int lancee in lancees) {
                frame.AddLancee(lancee);
            }

            Assert.Equal(resultat, frame.GetState);

        }

        [Theory]
        [InlineData(new int [] { 0, 0, 0, 0 }, 0)]
        [InlineData(new int [] { 5, 0, 5, 0 }, 10)]
        [InlineData(new int [] { 8, 1, 5, 2 }, 16)]
        public void ScorePartieSimple(int [] lancees, int resultat) {
            Partie partie = new Partie();

            foreach (int lancee in lancees) {
                partie.AddLancee(lancee);
            }

            Assert.Equal(resultat, partie.GetScore());

        }

        [Theory]
        [InlineData(new int [] { 10, 10, 10, 10 }, 90)]
        [InlineData(new int [] { 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10 }, 300)]
        [InlineData(new int [] { 4, 0, 10, 10 }, 34)]
        [InlineData(new int [] { 4, 0, 10, 10, 0, 0 }, 34)]
        [InlineData(new int [] { 4, 0, 10, 10, 0, 5 }, 44)]
        [InlineData(new int [] { 4, 0, 10, 10, 0, 5, 8, 2, 4, 0, 7, 0 }, 69)]
        public void ScorePartieComplexe(int [] lancees, int resultat) {
            Partie partie = new Partie();

            foreach (int lancee in lancees) {
                partie.AddLancee(lancee);
            }

            Assert.Equal(resultat, partie.GetScore());

        }

        [Theory]
        [InlineData(new int [] { 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10 })]
        [InlineData(new int [] { 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 0 })]
        [InlineData(new int [] { 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 5, 5 })]
        [InlineData(new int [] { 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 5, 0 })]
        [InlineData(new int [] { 10, 10, 10, 10, 10, 10, 10, 10, 10, 5, 0 })]
        [InlineData(new int [] { 10, 10, 10, 10, 10, 10, 10, 10, 10, 5, 5, 10 })]
        public void ExceptionFinDePartie(int [] lancees) {
            Partie partie = new Partie();

            for (int i = 0; i < lancees.Length; i++) {
                partie.AddLancee(lancees [i]);
            }

            Assert.Throws<Exception>(() => partie.AddLancee(5));

        }
    }
}
